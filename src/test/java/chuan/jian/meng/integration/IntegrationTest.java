package chuan.jian.meng.integration;

import com.spotify.docker.client.DefaultDockerClient;
import com.spotify.docker.client.DockerClient;
import com.spotify.docker.client.exceptions.DockerCertificateException;
import com.spotify.docker.client.messages.*;
import com.weibo.api.truman.docker.DockerServer;
import com.weibo.api.truman.docker.DockerServerConfig;
import com.weibo.api.truman.suite.Truman;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by meng on 16-6-1.
 */
public class IntegrationTest {

    @BeforeClass
    public static void before(){
        System.out.println("before");
    }

    @Test
    public void integrationTest(){
        System.out.println("IntegrationTest.integrationTest");
        String dockerImage = "oddpoet/hbase-cdh5:latest";
        int containerPort = 2181;
        int asyncStopDelay = 0;
        DockerServerConfig dockerServerConfig = DockerServerConfig.builder().serverType("HBASE").hostPort(2181).serverImage(dockerImage).containerPort(containerPort).secondsHoldOnBeforeAsyncStop(asyncStopDelay).build();
        DockerServer dockerServer = new DockerServer(dockerServerConfig);
        boolean startSucc = dockerServer.start();
        System.out.println(startSucc);
        System.out.println(dockerServer.getContainerInfo());
        try {
            Thread.sleep(10000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        dockerServer.stop();
        /*try {
            DockerClient docker = DefaultDockerClient.fromEnv().build();


            final String[] ports = {"2181", "2181"};
            final Map<String, List<PortBinding>> portBindings = new HashMap<String, List<PortBinding>>();
            for (String port : ports) {
                List<PortBinding> hostPorts = new ArrayList<PortBinding>();
                hostPorts.add(PortBinding.of("0.0.0.0", port));
                portBindings.put(port, hostPorts);
            }

// Bind container port 443 to an automatically allocated available host port.
//            List<PortBinding> randomPort = new ArrayList<PortBinding>();
//            randomPort.add(PortBinding.randomPort("0.0.0.0"));
//            portBindings.put("443", randomPort);

            final HostConfig hostConfig = HostConfig.builder().portBindings(portBindings).build();

            ContainerConfig containerConfig = ContainerConfig.builder()
                    .hostConfig(hostConfig)
                    .image("oddpoet/hbase-cdh5:latest")
                    .hostname("gitlab-com-test")
                    .exposedPorts(ports)
                    .cmd("sh", "-c", "while :; do sleep 1; done")
                    .build();
            ContainerCreation creation = docker.createContainer(containerConfig);
            String id = creation.id();
            ContainerInfo info = docker.inspectContainer(id);
            docker.startContainer(id);
            System.out.println(info);

            Thread.sleep(10000);
            // Kill container
            docker.killContainer(id);

            // Remove container
            docker.removeContainer(id);

            docker.close();*/

    }

    @AfterClass
    public static void after(){
        System.out.println("after");
    }


}
